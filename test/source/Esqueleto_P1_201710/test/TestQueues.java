package Esqueleto_P1_201710.test;

import model.data_structures.Queue;
import junit.framework.TestCase;

public class TestQueues extends TestCase
{
	private Queue queue;

	public void setupEscenario1(){
		Queue<String> cola= new Queue<String>();
		
	}
	public void testEnqueueYDequeue(){
		setupEscenario1();
		String elem= "Hola";
		queue.enqueue(elem);
		assertNotNull("Deberia agregar el elemento al final",queue.dequeue() );
		
	}
	public void testSize(){
		setupEscenario1();
		String elem= "H";
		String elem1= "O";
		String elem2= "L";
		String elem3= "A";
		queue.enqueue(elem);
		queue.enqueue(elem1);
		queue.enqueue(elem2);
		queue.enqueue(elem3);
		assertEquals("Se esperaba 4 y el resultado dio diferente",4,queue.size());
		
	}
	
}
