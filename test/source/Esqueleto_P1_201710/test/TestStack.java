package Esqueleto_P1_201710.test;

import junit.framework.TestCase;
import model.data_structures.Queue;
import model.data_structures.Stack;

public class TestStack extends TestCase {
	
	private Stack stack;

	public void setupEscenario1(){
		Stack<String> cola= new Stack<String>();
	}
	public void testPopYPush(){
		setupEscenario1();
		String elem= "Hola";
		stack.push(elem);
		assertNotNull("Deberia agregar el elemento al inicio",stack.pop());

	}
	public void testSize(){
		setupEscenario1();
		String elem= "H";
		String elem1= "O";
		String elem2= "L";
		String elem3= "A";
		stack.push(elem);
		stack.push(elem1);
		stack.push(elem2);
		stack.push(elem3);
		assertEquals("Se esperaba 4 y el resultado dio diferente",4,stack.size());

	}
}
