package Esqueleto_P1_201710.test;

import model.data_structures.ListaDobleEncadenada;
import junit.framework.TestCase;

public class TestListaDoble extends TestCase{

	//Atributos
	private ListaDobleEncadenada<String> lista;

	public void setupEscenario1(){
		ListaDobleEncadenada<String> listaDoble= new ListaDobleEncadenada<>();
	}

	public void testAgregarElementoFinal(){
		setupEscenario1();
		String elem= "Hola";
		String elem1= "Juju";
		lista.agregarElementoFinal(elem);
		assertNotNull("Se deberia haber agregado el elemento en la lista",lista.darElementoPosicionActual());

	}
	public void testDarElemento(){
		setupEscenario1();
		String elem= "H";
		String elem1= "O";
		String elem2= "L";
		String elem3= "A";
		lista.agregarElementoFinal(elem);
		lista.agregarElementoFinal(elem1);
		lista.agregarElementoFinal(elem2);
		lista.agregarElementoFinal(elem3);
		assertEquals("El elemento no es el esperado", "L", lista.darElemento(2));
	}
	public void testDarNumeroElementos(){
		setupEscenario1();
		String elem= "H";
		String elem1= "O";
		String elem2= "L";
		String elem3= "A";
		lista.agregarElementoFinal(elem);
		lista.agregarElementoFinal(elem1);
		lista.agregarElementoFinal(elem2);
		lista.agregarElementoFinal(elem3);
		assertEquals("El numero de elementos no es el esperado",4,lista.darNumeroElementos());
	}
	public void testDarElementoPosicionActual(){
		setupEscenario1();
		String elem= "H";
		String elem1= "O";
		String elem2= "L";
		String elem3= "A";
		lista.agregarElementoFinal(elem);
		lista.agregarElementoFinal(elem1);
		lista.agregarElementoFinal(elem2);
		lista.agregarElementoFinal(elem3);
		assertEquals("El elemento no es el esperado","A",lista.darElementoPosicionActual());
	}

}
