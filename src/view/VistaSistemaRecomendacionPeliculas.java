package view;

import java.util.Scanner;

import model.data_structures.ILista;
import model.data_structures.ListaDobleEncadenada;
import model.data_structures.ListaEncadenada;
import model.logic.Shell;
import model.logic.ShellLista;
import model.vo.*;
import controller.Controller;

public class VistaSistemaRecomendacionPeliculas {
	
public static void main(String[] args) {
		
		
		Scanner sc=new Scanner(System.in);
		boolean fin=false;
		while(!fin){
			printMenu();
			
			int option = sc.nextInt();
			
			switch(option){
				case 1:
					Controller.cargarPeliculas();			
					Controller.cargarRatings();
					Controller.cargarTags();
					Controller.cargarGeneros();
					
					
					ListaDobleEncadenada<VOPelicula> lista=new ListaDobleEncadenada<>();
					for(int i =0;i<15;i++)
					{
					VOPelicula peli1=new VOPelicula();
					peli1.setAgnoPublicacion(5);
					peli1.setPromedioRatings(i*5);
					peli1.setTitulo("tituloaspero"+i);
					lista.agregarElementoFinal(peli1);
					}
					ShellLista sort = new ShellLista();
					sort.sort(lista, lista.darNumeroElementos());
					for(int i=0; i< lista.darNumeroElementos(); i++){
						System.out.println(lista.darElemento(i).getTitulo());
					}
					break;
				case 2:
					
//					ListaDobleEncadenada<VOGeneroPelicula> lista1=(ListaDobleEncadenada<VOGeneroPelicula>) Controller.dar();
//					System.out.println(lista1.darNumeroElementos());
					ListaDobleEncadenada<VORating> tags = Controller.ratingsPeliculaSR(1);
					System.out.println(tags.darNumeroElementos());
					for(int i=0; i< tags.darNumeroElementos(); i++)
					{
						System.out.println(tags.darElemento(i).getIdUsuario()+"     "+tags.darElemento(i).getRating());
					}
					break;
				case 3:
					ListaDobleEncadenada<VOPelicula> lista3=new ListaDobleEncadenada<VOPelicula>();
					lista3=(ListaDobleEncadenada<VOPelicula>) Controller.catalogoPeliculasOrdenadoSR();
					for(int i=0; i< lista3.darNumeroElementos(); i++)
					{
						System.out.println(lista3.darElemento(i).getAgnoPublicacion()+"     "+lista3.darElemento(i).getPromedioRatings()+"     "+lista3.darElemento(i).getTitulo());
					}
					break;
				case 4:
					Controller.cargarPeliculas();			
					Controller.cargarRatings();
					Controller.cargarTags();
					Controller.cargarGeneros();
					ListaDobleEncadenada<VOGeneroPelicula> lista4=new ListaDobleEncadenada<VOGeneroPelicula>();
					lista4=(ListaDobleEncadenada<VOGeneroPelicula>) Controller.recomendarGenerosSR();
					for(int i=0; i< lista4.darNumeroElementos(); i++)
					{
						System.out.println(lista4.darElemento(i).getPeliculas().darElemento(i).getTitulo() + lista4.darNumeroElementos());
					}
					break;
				case 5:
					Controller.cargarPeliculas();			
					Controller.cargarRatings();
					Controller.cargarTags();
					ListaDobleEncadenada<VOGeneroPelicula> lista5=new ListaDobleEncadenada<VOGeneroPelicula>();
					lista5= Controller.opinionRatingGeneros();
					for(int i=0; i< lista5.darNumeroElementos(); i++){
						System.out.println(lista5.darElemento(i).getPeliculas().darElemento(i).getTitulo());
					}
//				case 2:
//					System.out.println("Ingrese subcadena de b�squeda:");
//					String busqueda=sc.next();
//					ILista<VOPelicula> lista=Controller.darListaPeliculas(busqueda);
//					System.out.println("Se encontraron "+lista.darNumeroElementos()+" elementos");
//					for (VOPelicula voPelicula : lista) {
//						System.out.println(voPelicula.getTitulo()+" "+voPelicula.getAgnoPublicacion());
//					}
//					break;
//				case 3:
//					System.out.println("Ingrese el a�o de b�squeda");
//					int agno=sc.nextInt();
//					ILista<VOPelicula> listaPeliculasAgno=Controller.darPeliculasPorAgno(agno);
//					System.out.println("Se encontraron "+listaPeliculasAgno.darNumeroElementos()+" elementos");
//					for (VOPelicula voPelicula : listaPeliculasAgno) {
//						System.out.println(voPelicula.getTitulo()+" "+voPelicula.getAgnoPublicacion());
//					}
//					break;
//				case 4:
//					VOAgnoPelicula agnoSiguiente=Controller.darPeliculasAgnoSiguiente();
//					System.out.println("Las pel�culas del a�o "+agnoSiguiente.getAgno()+"son ");
//					ILista<VOPelicula> listaPeliculasAgnoSiguiente=agnoSiguiente.getPeliculas();
//					for (VOPelicula voPelicula : listaPeliculasAgnoSiguiente) {
//						System.out.println(voPelicula.getTitulo()+" "+voPelicula.getAgnoPublicacion());
//					}
//					break;
//				case 5:
//					VOAgnoPelicula agnoAnterior=Controller.darPeliculasAgnoAnterior();
//					System.out.println("Las pel�culas del a�o "+agnoAnterior.getAgno()+"son ");
//					ILista<VOPelicula> listaPeliculasAgnoAnterior=agnoAnterior.getPeliculas();
//					for (VOPelicula voPelicula : listaPeliculasAgnoAnterior) {
//						System.out.println(voPelicula.getTitulo()+" "+voPelicula.getAgnoPublicacion());
//					}
//					break;
				case 6:	
					fin=true;
					break;
			}
			
			
		}
	}

	private static void printMenu() {
		System.out.println("---------ISIS 1206 - Estructuras de datos----------");
		System.out.println("---------------------Taller 2----------------------");
		System.out.println("1. CCree una nueva colecci�n de pel�culas (data/movies.csv)");
		System.out.println("2. Buscar pel�culas por subcadena");
		System.out.println("3. Buscar pel�culas por a�o");
		System.out.println("4. Dar pel�culas a�o siguiente");
		System.out.println("5. Dar pel�culas a�o anterior");
		System.out.println("6. Salir");
		System.out.println("Type the option number for the task, then press enter: (e.g., 1):");
		
	}


}
