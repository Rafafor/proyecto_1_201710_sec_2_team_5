package model.vo;

public class VOTag {
	private String tag;
	private long timestamp;
	private long idUsuario;
	private long idPelicula;
	
	public String getTag() {
		return tag;
	}
	public void setTag(String tag) {
		this.tag = tag;
	}
	public long getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}
	public void setIdPelicula(long idUsuario) {
		this.idPelicula = idUsuario;
	}
	public long getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(long idUsuario) {
		this.idUsuario = idUsuario;
	}
	
}
