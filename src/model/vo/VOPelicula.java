package model.vo;

import java.util.Comparator;

import model.data_structures.ILista;
import model.data_structures.ListaDobleEncadenada;

public class VOPelicula
{
	
	private long idPelicula;//cargado
	private String titulo;//cargado
	private int numeroRatings;//cargado
	private int numeroTags;//cargado
	private double promedioRatings;//cargado
	private int agnoPublicacion;//cargado

	private ILista<String> tagsAsociados=new ListaDobleEncadenada<>();//cargado
	private ILista<String> generosAsociados=new ListaDobleEncadenada<>();//cargado	
	private ILista<Double> calificacionesAsociadas = new ListaDobleEncadenada<>();

	
	public long getIdPelicula() {
		return idPelicula;
	}
	public void setIdPelicula(long idUsuario) {
		this.idPelicula = idUsuario;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public int getNumeroRatings() {
		return numeroRatings;
	}
	public void setNumeroRatings(int numeroRatings) {
		this.numeroRatings = numeroRatings;
	}
	public int getNumeroTags() {
		return numeroTags;
	}
	public void setNumeroTags(int numeroTags) {
		this.numeroTags = numeroTags;
	}
	public double getPromedioRatings() {
		return promedioRatings;
	}
	public void setPromedioRatings(double promedioRatings) {
		this.promedioRatings = promedioRatings;
	}
	public int getAgnoPublicacion() {
		return agnoPublicacion;
	}
	public void setAgnoPublicacion(int agnoPublicacion) {
		this.agnoPublicacion = agnoPublicacion;
	}
	public ILista<String> getTagsAsociados() {
		return tagsAsociados;
	}
	public void setTagsAsociados(ILista<String> tagsAsociados) {
		this.tagsAsociados = tagsAsociados;
	}
	public ILista<String> getGenerosAsociados() {
		return generosAsociados;
	}
	public void setGenerosAsociados(ILista<String> generosAsociados) {
		this.generosAsociados = generosAsociados;
	}

	public void aumentarRatings (){
		this.numeroRatings ++;
	}
	
	public void agregarCalificacion (double valor){
		calificacionesAsociadas.agregarElementoFinal(valor);
	}
	public void AgregarTagPeli (String elAgregar){
		tagsAsociados.agregarElementoFinal(elAgregar);
	}
	public ILista<Double> darCalificaciones (){
		return calificacionesAsociadas;
	}
	public void actualizarPromedio()
	{
		double contador=0;
		for(int i =0;i<calificacionesAsociadas.darNumeroElementos();i++)
		{
			contador+=calificacionesAsociadas.darElemento(i);
		}
		contador=contador/calificacionesAsociadas.darNumeroElementos();
		promedioRatings=contador;
	}
//	@Override
//	public int compareTo(VOPelicula o) {
//		// TODO Auto-generated method stub
//		return this.getAgnoPublicacion()-o.getAgnoPublicacion();
//	}
	

}
