package model.data_structures;

import java.util.Iterator;



public class ListaDobleEncadenada<T> implements ILista<T> {


	private NodoDoble<T> primero;
	private NodoDoble<T> ultimo;
	private NodoDoble<T> actual;


	public class IteratorListaDoble<T> implements Iterator<T>
	{
		private NodoDoble<T> actual;

		public IteratorListaDoble(NodoDoble<T> pActual)
		{
			actual=pActual;
		}

		public boolean hasNext() 
		{
			if (actual!=null)
			{
				return true;
			}
			return false;
		}

		public T next() 
		{
			NodoDoble<T> referencia = actual;
			T item = referencia.darElemento();
			actual = actual.darSiguiente(); 
			return item;
		}

		public void remove()
		{

		}
	}


	public ListaDobleEncadenada()
	{
		primero=null;
		ultimo=null;
		actual=primero;
	}	

	public Iterator<T> iterator() 
	{			
		return new IteratorListaDoble<T>(primero);
	}	

	public void agregarElementoFinal(T elem) 
	{			
		NodoDoble<T> elAgregar= new NodoDoble<T>(elem);

		if(primero==null)
		{
			primero= elAgregar;
			ultimo= primero;
		}
		else
		{
			ultimo.modificarSiguiente(elAgregar);
			elAgregar.modificarAnterior(ultimo);
			ultimo=elAgregar;
		}
	}	

	public void agregarElementoInicio(T elem)
	{
		NodoDoble<T> elAgregar= new NodoDoble<T>(elem);

		if(primero==null)
		{
			primero= elAgregar;
			ultimo= primero;
		}
		else
		{
			elAgregar.modificarSiguiente(primero);
			elAgregar.modificarAnterior(null);
			primero=elAgregar;
		}
	}

	public void cambiarDosElementos (int pos1, int pos2)
	{
		T temporal = darElemento(pos1);
		(darNodo(pos1)).modificarT(darElemento(pos2));
		(darNodo(pos2)).modificarT(temporal);
	}

	public T darElemento(int pos) 
	{

		int contador=0;
		if (primero==null){
			return null;
		}
		
		NodoDoble<T>buscado =primero;
		while (primero!=null && contador!=pos){
			buscado=buscado.darSiguiente();
			contador++;
		}

		actual=buscado;
		
		return buscado.darElemento();
	}	

	public NodoDoble<T> darNodo(int pos)
	{
		// TODO Auto-generated method stub

		int contador=0;
		if (primero==null){
			return null;
		}

		NodoDoble<T>buscado =primero;
		while (primero!=null && contador!=pos)
		{
			buscado=buscado.darSiguiente();
			contador++;
		}

		actual=buscado;

		return buscado;
	}

	public int darNumeroElementos() 
	{			
		NodoDoble<T> ahora=primero;
		int contador=0;
		while (ahora!=null) 
		{
			contador++;
			ahora=ahora.darSiguiente();

		}
		return contador;
	}


	public T darElementoPosicionActual() 
	{			
		if (actual==null)
		{
			actual=primero;
			return actual.darElemento();
		}

		return actual.darElemento();
	}


	public boolean avanzarSiguientePosicion() 
	{			
		if (primero==null)
		{
			return false;
		}

		else{

			if (actual==null)
			{
				actual=primero;
			}

			if (actual.darSiguiente()==null )
			{
				return false;
			}
			else
			{
				actual=actual.darSiguiente();
				return true;
			}
		}
	}

	public void moverActualInicio()
	{		
		actual=primero;			
	}

	public boolean retrocederPosicionAnterior()
	{			
		if (primero!=null)
		{
			return false;
		}
		else{
			if (actual==null)
			{
				actual=primero;
			}

			if (actual.darAnterior()==null )
			{
				return false;
			}
			else
			{
				actual=actual.darAnterior();
				return true;
			}
		}	
	}

	@Override
	public T eliminarElemento(int pos)
	{
		// TODO Auto-generated method stub
		if (darNumeroElementos()<=0)
		{
			System.out.println("No puede eliminar mas");
		}
		T aux = primero.darElemento();
		primero=primero.darSiguiente();
		return aux;
	}

	public T darPrimerElemento()
	{
		return primero.darElemento();
	}

	public T darElementoUltimo()
	{
		return ultimo.darElemento();
	}

	@Override
	public T darElemento(T elemento) {
		if (primero==null){
			return null;
		}
		NodoDoble<T>buscado =primero;
		while (buscado!=null ){
			if (buscado.darElemento().equals(elemento)){
				return buscado.darElemento();
			}
			buscado=buscado.darSiguiente();
		}

		return null;
	}

	@Override
	public void eliminarLista(){
		primero=null;
		actual=null;
	}


}
