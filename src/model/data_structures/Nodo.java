package model.data_structures;

public class Nodo<T> 
{
	private T elemento;
	private Nodo<T> siguiente;
	private Nodo<T> anterior;
	
	public Nodo(T pElemento)
	{
		elemento=pElemento;
		siguiente=null;
		anterior=null;
	}
	
	public T darElemento(){
		return elemento;
	}
	
	public void agregarElemento(T pElemento)
	{
		elemento= pElemento;
	}
	
	public Nodo<T> darSiguiente()
	{
		return siguiente;
	}
	
	public Nodo<T> darAnterior()
	{
		return anterior;
	}
	
	public void modificarSiguiente(Nodo<T> nodo)
	{
		siguiente=nodo;		
	}
	
	public void modificarAnterior(Nodo<T> nodo)
	{
		anterior=nodo;
	}
	
}
