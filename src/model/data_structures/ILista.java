package model.data_structures;

public interface ILista<T> extends Iterable<T>
{
	
	public void agregarElementoFinal(T elem);
	
	public T darElemento(int pos);
	
	public T eliminarElemento(int pos);
	
	public int darNumeroElementos();
	
	public T darElemento(T elemento);
	public T darPrimerElemento();
	
	public void eliminarLista();


}
