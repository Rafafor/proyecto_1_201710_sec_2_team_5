package model.data_structures;

import java.util.Iterator;

public class Stack<T> implements ILista<T>  {

	private int tamanio;
	private Nodo<T> primero;
	private Nodo<T> ultimo;

	public Stack()
	{
		tamanio=0;
		primero= null;
		ultimo= null;
	}

	
	@Override
	public T darElemento(int pos) {
		// TODO Auto-generated method stub
		Nodo<T> elemNodo= primero;
		for(int i=0; i <= pos; i++){
			elemNodo= elemNodo.darSiguiente();
		}
		return elemNodo.darElemento();
	}

	@Override
	public int darNumeroElementos() {
		// TODO Auto-generated method stub
		return tamanio;
	}


	public void push (T item){
		Nodo<T> oldfirst = primero;
		primero = new Nodo<T>(item);
		primero.agregarElemento(item);
		primero.modificarSiguiente(oldfirst);
		tamanio++;
	}

	public T pop (){
		T item = primero.darElemento();
		primero = primero.darSiguiente();
		return item;
	}

	public boolean isEmpty(){
		return primero==null;
	}

	public int size (){
		return darNumeroElementos();
	}
	@Override
	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void agregarElementoFinal(T elem) {
		// TODO Auto-generated method stub

	}


	@Override
	public T eliminarElemento(int pos) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public T darElemento(T elemento) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public T darPrimerElemento() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public void eliminarLista() {
		// TODO Auto-generated method stub
		
	}
}
