package model.data_structures;

import java.util.Iterator;

public class Queue <T> implements ILista<T>
{
	int tamanio;
	private Nodo<T> primero;
	private Nodo<T> ultimo;

	public Queue()
	{
		tamanio=0;
		primero= null;
		ultimo= null;

	}
	
	public void enqueue (T item)
	{
		agregarElementoFinal(item);
	}
	
	public T dequeue () 
	{
		T item = primero.darElemento();
		primero = primero.darSiguiente();
		if(isEmpty())
		{
			ultimo=null;
		}
		return item;
	}
	
	public boolean isEmpty()
	{
		return primero==null;

	}
	
	public int size()
	{
		return tamanio;
	}
	@Override
	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public void agregarElementoFinal(T elem) {
		// TODO Auto-generated method stub
		Nodo<T> ultimoAnterior= ultimo;
		ultimo= new Nodo<T>(elem);
		ultimo.agregarElemento(elem);
		ultimo.modificarSiguiente(null);
		
		if(isEmpty())
			primero= ultimo;
		else
		{
			ultimoAnterior.modificarSiguiente(ultimo);
		}
		tamanio++;
	}
	@Override
	public T darElemento(int pos) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public int darNumeroElementos() {
		// TODO Auto-generated method stub
		return 0;
	}


	@Override
	public T eliminarElemento(int pos) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public T darElemento(T elemento) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public T darPrimerElemento() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void eliminarLista() {
		// TODO Auto-generated method stub
		
	}
}
