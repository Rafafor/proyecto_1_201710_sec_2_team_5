package model.logic;

import java.util.Comparator;

import model.vo.VOGeneroPelicula;
import model.vo.VOPelicula;

public class ComparadorNumTagsPeliculas implements Comparator<VOPelicula>
{	

		@Override
		public int compare(VOPelicula arg0, VOPelicula arg1) 
		{
			return arg0.getNumeroTags()-arg1.getNumeroTags();
			
		}
			
		
}
