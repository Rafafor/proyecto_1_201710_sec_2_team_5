package model.logic;

import model.data_structures.ListaDobleEncadenada;
import model.vo.VOPelicula;

public class ShellNumTagsPelicula 
{
	public void sort(ListaDobleEncadenada<VOPelicula> lista,int N)
	{
		
		int h = 0;
		while(h < N/3)
		{
			h = 3*h+1;
		}
		while(h >= 1)
		{
			for (int i = h; i < N; i++)
			{
				for (int j = i; j >= h && less(lista.darElemento(j), lista.darElemento(j-h)); j-=h)
				{
					exch(lista,j,j-h);
				}
			}
			h=h/3;
		}
	}

	public boolean less(VOPelicula a, VOPelicula b)
	{
		ComparadorNumTagsPeliculas compa= new ComparadorNumTagsPeliculas();
		if (compa.compare(a, b)<0)
		{
			return true;
		}

		return false;
	}

	public void exch(ListaDobleEncadenada<VOPelicula> lista, int b, int c )
	{
		lista.cambiarDosElementos(b, c);
	}
}
