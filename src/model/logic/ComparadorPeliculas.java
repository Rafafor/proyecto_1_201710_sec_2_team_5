package model.logic;

import java.util.Comparator;

import model.vo.VOPelicula;
import model.vo.VOUsuario;

public class ComparadorPeliculas implements Comparator<VOPelicula>
{

	@Override
	public int compare(VOPelicula arg0, VOPelicula arg1) 
	{
		
			if (arg0.getAgnoPublicacion()-(arg1.getAgnoPublicacion())==0)
			{
				if (arg0.getPromedioRatings()-(arg1.getPromedioRatings())!=0)
				{
					if( (arg0.getPromedioRatings()-(arg1.getPromedioRatings())) >0)
					{
						return 1500;
					}
					else
					{
						return -1500;
					}
				}
				else
				{
					return arg0.getTitulo().compareTo(arg1.getTitulo());
				}
			}
			return arg0.getAgnoPublicacion()-(arg1.getAgnoPublicacion());
		
	}

	//	@Override
	//	public int compare(VOPelicula o1, VOUsuario o2) {
	//		// TODO Auto-generated method stub
	//		
	//		
	//		return (int) (o1.getPrimerTimestamp()-o2.getPrimerTimestamp());
	//	}
	//	
	//	public int compareRatings(VOUsuario o1, VOUsuario o2)
	//	{
	//		return (int) (o1.getNumRatings()-o2.getNumRatings());
	//	}	
	//	
	//	public int compareIdentificador(VOUsuario o1, VOUsuario o2)
	//	{
	//		return (int) (o1.getIdUsuario()-o2.getIdUsuario());
	//	}

}
