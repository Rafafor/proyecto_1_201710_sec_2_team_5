package controller;

import model.data_structures.*;
import model.vo.*;
import api.SistemaRecomendacionPeliculas;

public class Controller {
	private static SistemaRecomendacionPeliculas sistema=new SistemaRecomendacionPeliculas();
	
	public static ILista<VOGeneroPelicula> peliculasPopularesSR(int a)
	{
		return sistema.peliculasPopularesSR(a);
	}
	
	public static ILista<VOPelicula> catalogoPeliculasOrdenadoSR()
	{
		return sistema.catalogoPeliculasOrdenadoSR();
	}
	
	public static void cargarPeliculas()
	{
		sistema.cargarPeliculasSR("data/movies.csv");
	}
	
	public static void cargarRatings()
	{
		sistema.cargarRatingsSR("data/ratings.csv");
	}
	
	public static void cargarTags()
	{
		sistema.cargarTagsSR("data/tags.csv");

	}

	
	public static void cargarGeneros()
	{
		sistema.cargarPeliculasPorGenero();

	}
	public static ListaDobleEncadenada<VOGeneroPelicula> opinionRatingGeneros(){
		
		return (ListaDobleEncadenada<VOGeneroPelicula>) sistema.opinionRatingsGeneroSR();
	}
	
	public static ListaDobleEncadenada<VOGeneroPelicula> recomendarTagsGenerosSR(int n)
	{
		return (ListaDobleEncadenada<VOGeneroPelicula>) sistema.recomendarTagsGeneroSR(n);
	}
	
	public static ListaDobleEncadenada<VOGeneroPelicula> recomendarGenerosSR()
	{
		return (ListaDobleEncadenada<VOGeneroPelicula>) sistema.recomendarGeneroSR();
	}
	
	public static ListaDobleEncadenada<VOGeneroPelicula> dar()
	{
		return sistema.dar();
	}
	
	public static ListaDobleEncadenada<VOTag> tagsPeliculaSR(int idPelicula)
	{
		return (ListaDobleEncadenada<VOTag>) sistema.tagsPeliculaSR(idPelicula);
	}
	
	public static ListaDobleEncadenada<VOGeneroUsuario> usuariosActivosSR(int n)
	{
		return (ListaDobleEncadenada<VOGeneroUsuario>) sistema.usuariosActivosSR(n);
		
	}
	public static ListaDobleEncadenada<VORating> ratingsPeliculaSR(long idPelicula) 
	{
		return (ListaDobleEncadenada<VORating>) sistema.ratingsPeliculaSR(idPelicula);
		
	}
}
