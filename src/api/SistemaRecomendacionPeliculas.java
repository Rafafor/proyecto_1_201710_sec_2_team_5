package api;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Timestamp;

import org.omg.CORBA.portable.ValueOutputStream;

import sun.awt.shell.ShellFolder;
import model.data_structures.*;
import model.logic.ShellLista;
import model.logic.ShellNumTagsPelicula;
import model.vo.VOGeneroPelicula;
import model.vo.VOGeneroUsuario;
import model.vo.VOOperacion;
import model.vo.VOPelicula;
import model.vo.VOPeliculaPelicula;
import model.vo.VOPeliculaUsuario;
import model.vo.VORating;
import model.vo.VOTag;
import model.vo.VOUsuario;
import model.vo.VOUsuarioConteo;
import model.vo.VOUsuarioGenero;
import api.ISistemaRecomendacionPeliculas;

public class SistemaRecomendacionPeliculas implements ISistemaRecomendacionPeliculas
{

	ListaDobleEncadenada<VOPelicula> misPeliculas;
	ListaDobleEncadenada<VOTag> tags;
	ListaDobleEncadenada<VORating> ratings;
	ListaDobleEncadenada<VOUsuario> users;
	ListaDobleEncadenada<VOGeneroPelicula> generoPeliculasAsociadas;
	Stack<VOOperacion> operaciones= new Stack<VOOperacion>();
	long idPeliculaNueva=164980;

	public ListaDobleEncadenada<VOGeneroPelicula> dar()
	{
		return generoPeliculasAsociadas;
	}

	public boolean cargarPeliculasSR(String rutaPeliculas) 
	{		
		long timestampInicio =System.currentTimeMillis();
		System.out.println(timestampInicio);
		misPeliculas=new ListaDobleEncadenada<VOPelicula>();
		ratings = new ListaDobleEncadenada<>();
		tags = new ListaDobleEncadenada<>();
		users = new ListaDobleEncadenada<>();
		generoPeliculasAsociadas = new ListaDobleEncadenada<>();
		String csvFile = rutaPeliculas;
		String line = "";
		String cvsSplitBy = ",";

		try (BufferedReader br = new BufferedReader(new FileReader(csvFile))) 
		{
			String sinUso= br.readLine();
			while ((line = br.readLine()) != null) 
			{
				// use comma as separator
				String[] linea = line.split(cvsSplitBy);//0 cod, 1 nombrea�o, 2 genero
				String nombreanio="";
				String generosadiv="";

				if(linea[1].startsWith("\""))
				{
					String[] comacomilla=line.split(","+"\"");
					String[] comillacoma=comacomilla[1].split("\""+",");
					nombreanio=comillacoma[0];
					generosadiv=comillacoma[1];

				}
				else
				{
					nombreanio=linea[1];
					generosadiv=linea[2];
				}

				String nombre;
				String a�o;
				CharSequence cs1 =")";

				if(nombreanio.contains(cs1))
				{
					String[] lineaNA = nombreanio.split("\\(");//0 nombre 1 ... lineaNA.length -1 a�o
					a�o= lineaNA[lineaNA.length-1].substring(0,4);
					String[] nom= nombreanio.split("\\("+ a�o + "\\)");				
					nombre=nom[0].substring(0,nom[0].length()-1);	
				}
				else
				{
					nombre =nombreanio;
					a�o="0";
				}

				String[] lineaG =generosadiv.split("\\|"); 
				ListaDobleEncadenada<String> generos=new ListaDobleEncadenada<String>();
				for(int i =0;i<lineaG.length;i++)
				{
					generos.agregarElementoFinal(lineaG[i]);
				}

				VOPelicula aAgregar = new VOPelicula();
				aAgregar.setIdPelicula(Long.parseLong(linea[0]));
				aAgregar.setTitulo(nombre);
				aAgregar.setAgnoPublicacion(Integer.parseInt(a�o));
				aAgregar.setGenerosAsociados(generos);

				misPeliculas.agregarElementoFinal(aAgregar);
				//				System.out.println( aAgregar.getAgnoPublicacion()+" " + aAgregar.getTitulo()+" ");


			}
			//			System.out.println(misPeliculas.darNumeroElementos());
			cargarPeliculasPorGenero();
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
			return false;
		}
		long timestampFinal =System.currentTimeMillis();
		System.out.println(timestampFinal);

		return true;	

	}

	@Override
	public boolean cargarRatingsSR(String rutaRatings) 
	{
		long timestampInicio =System.currentTimeMillis();
		System.out.println(timestampInicio);
		// TODO Auto-generated method stub
		FileReader reader;
		try {	

			long userId=0L;
			long movieId=0L;
			double rating=0.0;
			long time=0L;
			reader = new FileReader(rutaRatings);	
			BufferedReader input = new BufferedReader(reader);


			try {
				String data= input.readLine();
				data=input.readLine();

				while (data!=null) {

					String [] subCadena = data.split(",");
					userId = Long.parseLong(subCadena [0]);
					movieId = Long.parseLong(subCadena[1]);
					rating = Double.parseDouble(subCadena [2]);
					time = Long.parseLong(subCadena[3]);

					VORating elemento = new VORating();
					VOUsuario user= new VOUsuario();

					VOPelicula peli = buscarPelicula(movieId);
					if (peli!=null){

						peli.aumentarRatings();
						peli.agregarCalificacion(rating);
						peli.actualizarPromedio();
					}

					user.setIdUsuario(userId);
					user.aumentarRaitings();
					user.setPrimerTimestamp(time);
					users.agregarElementoFinal(user);


					elemento.setIdPelicula(movieId);
					elemento.setIdUsuario(userId);
					elemento.setRating(rating);
					ratings.agregarElementoFinal(elemento);


					data=input.readLine();
				}


				reader.close();


			} catch (IOException e) {
				// TODO Auto-generated catch block

				System.out.println("ERROR READ LINE");
				return false;
			}


		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block

			System.out.println("ERROR FILE READER");
			return false;
		}
		long timestampFinal =System.currentTimeMillis();
		System.out.println(timestampFinal);

		return true;
	}

	@Override
	public boolean cargarTagsSR(String rutaTags)
	{
		long timestampInicio =System.currentTimeMillis();
		System.out.println(timestampInicio);
		// TODO Auto-generated method stub
		FileReader reader;
		try {	

			long userId=0L;
			long movieId=0L;
			String tag ="";
			long time=0L;
			reader = new FileReader(rutaTags);	
			BufferedReader input = new BufferedReader(reader);


			try {
				String data= input.readLine();
				data=input.readLine();

				while (data!=null) {

					String [] subCadena = data.split(",");
					userId = Long.parseLong(subCadena [0]);
					movieId = Long.parseLong(subCadena[1]);
					tag = subCadena[2];
					try{
						time = Long.parseLong(subCadena[3]);
					}
					catch (NumberFormatException e){
						time = 0L;
					}


					VOTag pelisTag = new VOTag();

					pelisTag.setIdPelicula(movieId);
					pelisTag.setIdUsuario(userId);
					VOUsuario user = buscarUsuario(userId);
					if (user!=null){
						user.agregarTagAlUsuario(pelisTag);
					}
					VOPelicula peli = buscarPelicula(movieId);
					if (peli!=null){

						peli.AgregarTagPeli(tag);
					}

					pelisTag.setTimestamp(time);
					pelisTag.setTag(tag);
					tags.agregarElementoFinal(pelisTag);


					data=input.readLine();
				}


				reader.close();


			} catch (IOException e) {
				// TODO Auto-generated catch block

				System.out.println("ERROR READ LINE");
				return false;
			}


		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block

			System.out.println("ERROR FILE READER");
			return false;
		}
		long timestampFinal =System.currentTimeMillis();
		System.out.println(timestampFinal);

		return true;
	}

	@Override
	public int sizeMoviesSR() 
	{
		return misPeliculas.darNumeroElementos();
	}

	@Override
	public int sizeUsersSR()
	{		
		return users.darNumeroElementos();
	}

	@Override
	public int sizeTagsSR() 
	{	
		return tags.darNumeroElementos();
	}
	public VOPelicula darElementoMayor( VOPelicula esMayor, ILista<VOPelicula> Lista){

		ILista<VOPelicula> listaPeliculas = new ListaDobleEncadenada<>();
		listaPeliculas = Lista;
		VOPelicula mayor = null;

		int comparar=0;

		if(esMayor==null){

		}
		else
		{
			esMayor.setNumeroRatings(0);
		}

		for (VOPelicula elemento : listaPeliculas) {


			if (comparar < elemento.getNumeroRatings() ){
				comparar = elemento.getNumeroRatings();
				mayor = elemento;


			}
		}

		if (comparar == 0){
			mayor = listaPeliculas.darPrimerElemento();
		}

		return mayor;
	}
	public void cargarPeliculasPorGenero(){
		ILista<String> peliculas = new ListaDobleEncadenada<>();

		for (VOPelicula pelicula :misPeliculas) {
			for (String genero : pelicula.getGenerosAsociados()) {
				if (peliculas.darElemento(genero)==null){
					peliculas.agregarElementoFinal(genero);

				}
			}
		}


		for (String generoString : peliculas) {
			VOGeneroPelicula peliculasGenero = new VOGeneroPelicula();
			ILista<VOPelicula> pelis = new ListaDobleEncadenada<>();
			peliculasGenero.setGenero(generoString);
			for (VOPelicula elemento : misPeliculas) {
				for (String genero : elemento.getGenerosAsociados()) {
					if (genero.equals(generoString)){
						pelis.agregarElementoFinal(elemento);
					}
				}
			}
			peliculasGenero.setPeliculas(pelis);
			generoPeliculasAsociadas.agregarElementoFinal(peliculasGenero);
		}



	}
	@Override
	public ILista<VOGeneroPelicula> peliculasPopularesSR(int n) {
		// TODO Auto-generated method stub
		long timestampInicio = System.currentTimeMillis();

		ListaDobleEncadenada<VOGeneroPelicula> peliculasPopulares= new ListaDobleEncadenada<>();

		for (VOGeneroPelicula elemento : generoPeliculasAsociadas){
			int contador =0;
			ILista<VOPelicula> peliculas = new ListaDobleEncadenada<>();

			VOGeneroPelicula elem = new VOGeneroPelicula();

			elem.setGenero(elemento.getGenero());

			VOPelicula mayor=null;

			while (contador<n ) {

				mayor = darElementoMayor(mayor, elemento.getPeliculas());
				peliculas.agregarElementoFinal(mayor);

				contador ++;
			}

			elem.setPeliculas(peliculas);

			peliculasPopulares.agregarElementoFinal(elem);

		}

		long timestampFinal =System.currentTimeMillis();
		agregarOperacionSR("peliculasPopulares", timestampFinal, timestampInicio);
		return peliculasPopulares;
	}


	@Override
	public ILista<VOPelicula> catalogoPeliculasOrdenadoSR() {
		// TODO Auto-generated method stub
		long timestampInicio =System.currentTimeMillis();
		ListaDobleEncadenada<VOPelicula> pelis= new ListaDobleEncadenada<>();
		pelis= misPeliculas;
		//Se ordena por a�o
		ShellLista sort= new ShellLista();
		sort.sort(pelis, pelis.darNumeroElementos());

		long timestampFinal =System.currentTimeMillis();
		agregarOperacionSR("catalogoPeliculasOrdenado", timestampFinal, timestampInicio);
		return pelis;
	}

	@Override
	//Mejores peliculas por genero
	public ILista<VOGeneroPelicula> recomendarGeneroSR() {
		// TODO Auto-generated method stub
		long timestampInicio =System.currentTimeMillis();
		cargarPeliculasPorGenero();
		ILista<VOGeneroPelicula> listaARecomendar = new ListaDobleEncadenada<>();

		for (VOGeneroPelicula elemento : generoPeliculasAsociadas){
			int contador =0;
			ILista<VOPelicula> peliculas = new ListaDobleEncadenada<>();

			VOGeneroPelicula elem = new VOGeneroPelicula();

			elem.setGenero(elemento.getGenero());

			VOPelicula mayor=null;



			mayor = darElementoMayor(mayor, elemento.getPeliculas());
			if (mayor ==  null || mayor.getTitulo().equals(""))
			{

				mayor = darElementoMayor(mayor, elemento.getPeliculas());
			}
			peliculas.agregarElementoFinal(mayor);



			elem.setPeliculas(peliculas);

			listaARecomendar.agregarElementoFinal(elem);

		}
		long timestampFinal =System.currentTimeMillis();
		agregarOperacionSR("RecomendarGenero", timestampFinal, timestampInicio);

		return listaARecomendar;

	}

	@Override
	public ILista<VOGeneroPelicula> opinionRatingsGeneroSR() {
		// TODO Auto-generated method stub
		long timestampInicio= System.currentTimeMillis();
		for (VOGeneroPelicula genero :generoPeliculasAsociadas) {
			ShellLista ordenar= new ShellLista();
			ordenar.sort((ListaDobleEncadenada<VOPelicula>) genero.getPeliculas(), genero.getPeliculas().darNumeroElementos());
		}
		long timestampFinal =System.currentTimeMillis();
		agregarOperacionSR("ratingsPelicula", timestampFinal, timestampInicio);

		return generoPeliculasAsociadas;
	}

	@Override
	public ILista<VOPeliculaPelicula> recomendarPeliculasSR(
			String rutaRecomendacion, int n) {
		// TODO Auto-generated method stub
		long timestampInicio= System.currentTimeMillis();
		FileReader lector;
		ILista<VOPeliculaPelicula> listaFinal = new ListaDobleEncadenada<>();
		listaFinal.eliminarLista();

		try {	

			long userID=0L;
			double rating=0.0;

			lector = new FileReader(rutaRecomendacion);	
			BufferedReader input = new BufferedReader(lector);


			try {
				String data= input.readLine();
				data=input.readLine();
				int contador = 0;
				while (data!=null && contador<n) {
					ILista<VOPelicula> listaRelacionadas= new ListaDobleEncadenada<>();
					VOPeliculaPelicula tip = new VOPeliculaPelicula();
					String [] subCadena = data.split(",");
					userID = Long.parseLong(subCadena [0]);
					rating = Double.parseDouble(subCadena[1]);
					VOPelicula peli =buscarPelicula(userID);
					if (peli!=null){
						String generoPrincipal = (peli.getGenerosAsociados().darPrimerElemento());
						for (VOGeneroPelicula generos : generoPeliculasAsociadas) {
							if (generos.getGenero().equals(generoPrincipal)){
								for (VOPelicula subPelis : generos.getPeliculas()) {
									if ((peli.getPromedioRatings()-subPelis.getPromedioRatings())<=5 ){
										listaRelacionadas.agregarElementoFinal(subPelis);
									}
								}
							}
						}

						tip.setPelicula(peli);
						tip.setPeliculasRelacionadas(listaRelacionadas);
						listaFinal.agregarElementoFinal(tip);
					}



					contador ++;
					data=input.readLine();
				}


				lector.close();



			} catch (IOException e) {
				// TODO Auto-generated catch block

				System.out.println("ERROR READ LINE");

			}


		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block

			System.out.println("ERROR FILE READER");

		}
		long timestampFinal =System.currentTimeMillis();
		agregarOperacionSR("ratingsPelicula", timestampFinal, timestampInicio);

		return listaFinal;


	}

	@Override
	public ILista<VORating> ratingsPeliculaSR(long idPelicula) {
		// TODO Auto-generated method stub
		long timestampInicio= System.currentTimeMillis();
		// TODO Auto-generated method stub
		ILista<VORating> listaFinal = new ListaDobleEncadenada<>();
		VOPelicula peli = buscarPelicula(idPelicula);
		if (peli!=null){

			for (Double voRating : peli.darCalificaciones()) {
				VORating nuevo = new VORating();
				nuevo.setIdPelicula(idPelicula);
				nuevo.setRating(voRating);
				listaFinal.agregarElementoFinal(nuevo);
			}
		}

		long timestampFinal =System.currentTimeMillis();
		agregarOperacionSR("ratingsPelicula", timestampFinal, timestampInicio);


		return listaFinal;
	}

	@Override
	public ILista<VOGeneroUsuario> usuariosActivosSR(int n)
	{
		long timestampInicio= System.currentTimeMillis();
		ListaDobleEncadenada<VOGeneroUsuario> activos = new ListaDobleEncadenada<VOGeneroUsuario>();

		//recorre los ratings
		for (int i=0;i<ratings.darNumeroElementos();i++)
		{
			ListaDobleEncadenada<VOUsuarioConteo> conteo= new ListaDobleEncadenada<VOUsuarioConteo>();
			VORating rat =ratings.darElemento(i);
			//recorre las peliculas buscando la que pelicula a la que se le halla hecho el rating
			for (int j=0;j<misPeliculas.darNumeroElementos();j++)
			{
				VOPelicula peli=misPeliculas.darElemento(j);
				if(rat.getIdPelicula()==peli.getIdPelicula())//es la pelicula?
				{
					ListaDobleEncadenada<String> generos=(ListaDobleEncadenada<String>) peli.getGenerosAsociados();
					//se recorren los generos de la pelicula encopntrada
					for(int k=0;k<generos.darNumeroElementos();k++)
					{
						String genero=generos.darElemento(k);
						VOUsuarioConteo user = new VOUsuarioConteo();

						if(activos.darNumeroElementos()==0)//esta vacia la lista de vogenerousuarios?
						{
							user.setConteo(1);
							user.setIdUsuario(rat.getIdUsuario());

							conteo.agregarElementoFinal(user);

							VOGeneroUsuario aAgregar = new VOGeneroUsuario();
							aAgregar.setGenero(genero);
							aAgregar.setUsuarios(conteo);

							activos.agregarElementoFinal(aAgregar);
						}
						else//no esta vacia buscar el genero, el usuario correspondiente y sumarle un rating
						{	
							//Se recorren los generoususario actuales
							for(int l=0;l<activos.darNumeroElementos();l++)
							{
								VOGeneroUsuario act=activos.darElemento(l);
								//encuentra el genero correspondiente al rating
								if(genero.equals(act.getGenero()))
								{
									ListaDobleEncadenada<VOUsuarioConteo> usuarios=(ListaDobleEncadenada<VOUsuarioConteo>) act.getUsuarios();
									boolean auxhallado =false;
									//hallar al usuario correspondiente y sumarle un rating
									for(int e=0;e<usuarios.darNumeroElementos();e++)
									{										
										VOUsuarioConteo usuario=usuarios.darElemento(e);
										if(usuario.getIdUsuario()==rat.getIdUsuario())
										{
											usuario.setConteo(usuario.getConteo()+1);
											auxhallado=true;
										}
									}
									//si no halla al usuario debe crearlo
									if(!auxhallado)
									{
										user.setConteo(1);
										user.setIdUsuario(rat.getIdUsuario());

										conteo=(ListaDobleEncadenada<VOUsuarioConteo>) act.getUsuarios();
										conteo.agregarElementoFinal(user);
										act.setUsuarios(conteo);										
									}
								}
							}	
						}
					}
				}	
			}

		}

		long timestampFinal =System.currentTimeMillis();
		agregarOperacionSR("usuariosActivos", timestampFinal, timestampInicio);
		return activos;
	}

	@Override
	public ILista<VOUsuario> catalogoUsuariosOrdenadoSR()
	{
		long timestampInicio= System.currentTimeMillis();
		long timestampFinal =System.currentTimeMillis();
		agregarOperacionSR("catalogoUsuariosOrdenado", timestampFinal, timestampInicio);

		return null;
	}


	@Override
	public ILista<VOGeneroPelicula> recomendarTagsGeneroSR(int n) {
		long timestampInicio= System.currentTimeMillis();
		ListaDobleEncadenada<VOGeneroPelicula> gPelis= new ListaDobleEncadenada<>();

		// recorre la lista de generos con su lista de pel;iculas correspondientes
		for (int i=0;i<generoPeliculasAsociadas.darNumeroElementos();i++)
		{
			VOGeneroPelicula genero=generoPeliculasAsociadas.darElemento(i);
			ListaDobleEncadenada<VOPelicula> pelis =(ListaDobleEncadenada<VOPelicula>) genero.getPeliculas();
			//Ordena la lista por numero de tags de menor a mayor
			ShellNumTagsPelicula sort= new ShellNumTagsPelicula();
			sort.sort(pelis, pelis.darNumeroElementos());
			//Se saca el ultimo elemento de la lista ya ordenada, o sea el que tiene mayor numeros de tags
			VOPelicula maxTags= pelis.darElemento(pelis.darNumeroElementos()-1);
			//Se agrega la pelicula con el genero correspondiente a la lista de VOGeneroPelicula
			VOGeneroPelicula gPeliE = new VOGeneroPelicula();
			ListaDobleEncadenada<VOPelicula> gListapelis=new ListaDobleEncadenada<VOPelicula>();
			gListapelis.agregarElementoFinal(maxTags);
			gPeliE.setGenero(genero.getGenero());
			gPeliE.setPeliculas(gListapelis);			
			gPelis.agregarElementoFinal(gPeliE);

		}
		long timestampFinal =System.currentTimeMillis();
		agregarOperacionSR("recomendarTagsGenero", timestampFinal, timestampInicio);
		return gPelis;
	}

	@Override
	public ILista<VOUsuarioGenero> opinionTagsGeneroSR() {
		// TODO Auto-generated method stub
		long timestampInicio= System.currentTimeMillis();
		long timestampFinal =System.currentTimeMillis();
		agregarOperacionSR("opinionTagsGenero", timestampFinal, timestampInicio);
		return null;
	}

	@Override
	public ILista<VOPeliculaUsuario> recomendarUsuariosSR(
			String rutaRecomendacion, int n) {
		long timestampInicio= System.currentTimeMillis();
		long timestampFinal =System.currentTimeMillis();
		agregarOperacionSR("recomendarUsuarios", timestampFinal, timestampInicio);
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ILista<VOTag> tagsPeliculaSR(int idPelicula) {
		// TODO Auto-generated method stub
		long timestampInicio= System.currentTimeMillis();
		ILista<VOTag> tags = new ListaDobleEncadenada<>();
		VOPelicula peli = buscarPelicula(idPelicula);

		if (peli!=null)		{

			for (String voRating : peli.getTagsAsociados()) 
			{
				VOTag nuevo = new VOTag();
				nuevo.setIdPelicula(idPelicula);
				nuevo.setTag(voRating);
				tags.agregarElementoFinal(nuevo);
			}
		}

		long timestampFinal =System.currentTimeMillis();
		agregarOperacionSR("tagsPelicula", timestampFinal, timestampInicio);
		return tags;
	}

	@Override
	public void agregarOperacionSR(String nomOperacion, long tinicio, long tfin) {

		VOOperacion op= new VOOperacion();
		op.setTimestampInicio(tinicio);
		op.setTimestampFin(tfin);
		op.setOperacion(nomOperacion);
		operaciones.push(op);
	}

	@Override
	public ILista<VOOperacion> darHistoralOperacionesSR() {

		return operaciones;
	}

	@Override
	public void limpiarHistorialOperacionesSR() {

		operaciones = new Stack<VOOperacion>();

	}

	@Override
	public ILista<VOOperacion> darUltimasOperaciones(int n) {

		ListaDobleEncadenada<VOOperacion> ultimasOperaciones = new ListaDobleEncadenada<VOOperacion>();
		for(int i = 0;i<n;i++)
		{
			ultimasOperaciones.agregarElementoFinal(operaciones.pop());
		}
		return ultimasOperaciones;
	}

	@Override
	public void borrarUltimasOperaciones(int n) {

		for(int i = 0;i<n;i++)
		{
			operaciones.pop();
		}

	}

	@Override
	public long agregarPelicula(String titulo, int agno, String[] generos) {
		// TODO Auto-generated method stub
		VOPelicula aAgregar = new VOPelicula();
		ListaDobleEncadenada<String> listaGeneros = new ListaDobleEncadenada<String>();
		aAgregar.setAgnoPublicacion(agno);
		aAgregar.setTitulo(titulo);
		for(int i = 0;i<generos.length;i++)
		{
			listaGeneros.agregarElementoFinal(generos[i]);
		}
		aAgregar.setGenerosAsociados(listaGeneros);
		aAgregar.setIdPelicula(idPeliculaNueva);
		idPeliculaNueva++;

		return aAgregar.getIdPelicula();
	}

	@Override
	public void agregarRating(int idUsuario, int idPelicula, double rating) {
		// TODO Auto-generated method stub
		VORating aAgregar = new VORating();
		aAgregar.setIdUsuario(idUsuario);
		aAgregar.setIdPelicula(idPelicula);
		aAgregar.setRating(rating);
		ratings.agregarElementoFinal(aAgregar);

	}

	public VOPelicula buscarPelicula (long id){
		for (VOPelicula elemto : misPeliculas) {
			if (elemto.getIdPelicula()==id){
				return elemto;
			}
		}
		return null;
	}

	public VOUsuario buscarUsuario (long id){
		for (VOUsuario elemento : users) {
			if (elemento.getIdUsuario()==id)
			{
				return elemento;
			}
		}
		return null;
	}
}